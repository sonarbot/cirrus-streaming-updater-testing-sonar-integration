package org.wikimedia.discovery.cirrus.updater.common.wiremock;

import java.net.URISyntaxException;
import java.util.Collections;
import java.util.MissingResourceException;

import com.github.jknack.handlebars.Handlebars;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import com.github.tomakehurst.wiremock.extension.responsetemplating.ResponseTemplateTransformer;
import com.github.tomakehurst.wiremock.junit5.WireMockExtension;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.experimental.UtilityClass;

@UtilityClass
@SuppressWarnings("HideUtilityClassConstructor")
@SuppressFBWarnings(
        value = "HideUtilityClassConstructor",
        justification = "lombok takes care of that")
public class WireMockExtensionUtilities {

    private static final String ROOT_DIRECTORY;

    static {
        try {
            ROOT_DIRECTORY = WireMockExtensionUtilities.class.getResource("/wiremock").toURI().getPath();
        } catch (URISyntaxException e) {
            throw new MissingResourceException(
                    "Unable to resolve wiremock root directory",
                    WireMockExtensionUtilities.class.getCanonicalName(),
                    "/wiremock");
        }
    }

    private static final Handlebars HANDLEBARS = new Handlebars();

    static {
        // override default ({{) so handlebars does not get confused with wiki text expressions
        HANDLEBARS.setStartDelimiter("${{");
    }

    public static WireMockExtension getWireMockExtension() {
        return WireMockExtension.newInstance().options(getWireMockConfiguration()).build();
    }

    public static WireMockConfiguration getWireMockConfiguration() {
        return WireMockConfiguration.options()
                .withRootDirectory(ROOT_DIRECTORY)
                .dynamicPort()
                .extensions(
                        new ResponseTemplateTransformer(true, HANDLEBARS, Collections.emptyMap(), null, null));
    }
}
