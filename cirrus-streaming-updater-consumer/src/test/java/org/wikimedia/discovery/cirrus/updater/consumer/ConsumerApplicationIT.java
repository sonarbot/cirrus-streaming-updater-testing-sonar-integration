package org.wikimedia.discovery.cirrus.updater.consumer;

import static org.assertj.core.api.Assertions.assertThat;
import static org.wikimedia.discovery.cirrus.updater.common.wiremock.WireMockExtensionUtilities.getWireMockExtension;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.function.Function;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.apache.commons.io.FileUtils;
import org.apache.flink.test.junit5.MiniClusterExtension;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.LongSerializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testcontainers.containers.output.OutputFrame.OutputType;
import org.testcontainers.elasticsearch.ElasticsearchContainer;
import org.testcontainers.utility.DockerImageName;
import org.wikimedia.discovery.cirrus.updater.common.serde.EventDataStreamUtilities;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.junit5.WireMockExtension;
import com.github.tomakehurst.wiremock.recording.RecordSpec;
import com.github.tomakehurst.wiremock.recording.SnapshotRecordResult;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.salesforce.kafka.test.junit5.SharedKafkaTestResource;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@ExtendWith({MiniClusterExtension.class})
@SuppressWarnings("checkstyle:classfanoutcomplexity")
class ConsumerApplicationIT {

    @RegisterExtension
    public static final SharedKafkaTestResource SHARED_KAFKA_TEST_RESOURCE =
            new SharedKafkaTestResource();

    @RegisterExtension static final WireMockExtension wireMockExtension = getWireMockExtension();

    /**
     * Determines if an instance of elasticsearch should be launched.
     *
     * <p>If {@code PROXY_ELASTICSEARCH=true}, launch an instance of elasticsearch and use a wiremock
     * proxy to record requests to it. Otherwise, use only wiremock to stub responses loaded from
     * {@code /wiremock/mappings}.
     *
     * <p>Recordings will be stored under {@code target/test-classes/wiremock/mappings}.
     */
    private static final boolean PROXY_ELASTICSEARCH =
            "true".equals(System.getenv("PROXY_ELASTICSEARCH"));

    public static final String REV_TIMESTAMP_FIELD = "rev_timestamp";
    private static final String ELASTICSEARCH_PREFIX = "/elasticsearch";

    private static final ElasticsearchContainer elasticsearchContainer =
            new ElasticsearchContainer(getElasticSearchDockerImageName())
                    .withEnv(
                            ImmutableMap.of(
                                    "discovery.type", "single-node",
                                    "bootstrap.system_call_filter", "false"))
                    .withLogConsumer(
                            frame -> {
                                final Logger logger = LoggerFactory.getLogger("ES");
                                if (frame.getType() == OutputType.STDERR) {
                                    logger.error(frame.getUtf8String());
                                } else {
                                    logger.info(frame.getUtf8String());
                                }
                            });

    /**
     * Returns the operation-system-architecture-dependent docker image. Currently, there is no
     * universal image provided by WMF, see <a
     * href="https://www.mediawiki.org/wiki/MediaWiki-Docker/Configuration_recipes/ElasticSearch">documentation</a>.
     */
    @NotNull
    private static DockerImageName getElasticSearchDockerImageName() {
        final String defaultImageName =
                "docker-registry.wikimedia.org/dev/cirrus-elasticsearch:7.10.2-s0";
        final String armImageName = "kostajh/wmf-elasticsearch-arm64:7.10.2";
        final boolean arm = System.getProperty("os.arch").toLowerCase(Locale.US).startsWith("aa");
        return DockerImageName.parse(arm ? armImageName : defaultImageName)
                .asCompatibleSubstituteFor("docker.elastic.co/elasticsearch/elasticsearch");
    }

    @BeforeAll
    static void startElasticSearch() {
        if (PROXY_ELASTICSEARCH) {
            elasticsearchContainer.start();
        }
    }

    @AfterAll
    static void stopElasticSearch() {
        if (PROXY_ELASTICSEARCH) {
            elasticsearchContainer.stop();
        }
    }

    @Test
    void test() throws Exception {
        final Instant now = Instant.now();
        final Instant endWM = now.plusSeconds(10);

        final String updateTopicName = "eqiad.cirrussearch.update_pipeline.update";

        try (AdminClient adminClient =
                SHARED_KAFKA_TEST_RESOURCE.getKafkaTestUtils().getAdminClient()) {
            final NewTopic updateTopic = new NewTopic(updateTopicName, 1, (short) 1);
            // TODO: Do we need to tell the broker about compression?
            //  By default its configured to accept whatever the producer specifies via compression.type.
            //  So more importantly the producing client must define compression.type.
            updateTopic.configs(ImmutableMap.of("compression.type", "producer"));
            adminClient.createTopics(ImmutableSet.of(updateTopic));
        }

        final Function<JsonNode, JsonNode> jsonNodeJsonNodeFunction =
                allEventsBefore(now, Duration.ofSeconds(-1));

        List<Instant> inputMessageTimestamps =
                produceUpdateRecords(jsonNodeJsonNodeFunction, updateTopicName, endWM);

        String elasticsearchWireMockUrl = getElasticsearchUrl();

        ConsumerApplication.main(
                new String[] {
                    "--event-stream-config-url",
                    this.getClass().getResource("/event-stream-config.json").toString(),
                    "--event-stream-json-schema-urls",
                    this.getClass().getResource("/schema_repo").toString(),
                    "--update-stream",
                    "cirrussearch.update_pipeline.update",
                    "--kafka-source-config.bootstrap.servers",
                    SHARED_KAFKA_TEST_RESOURCE.getKafkaConnectString(),
                    "--kafka-source-config.group.id",
                    ConsumerApplicationIT.class.getSimpleName(),
                    "--kafka-source-start-time",
                    inputMessageTimestamps.stream().min(Instant::compareTo).map(Instant::toString).get(),
                    "--kafka-source-end-time",
                    inputMessageTimestamps.stream().max(Instant::compareTo).map(Instant::toString).get(),
                    "--elasticsearch-urls",
                    elasticsearchWireMockUrl,
                });

        verifyElasticsearch();
    }

    private static void verifyElasticsearch() {
        if (PROXY_ELASTICSEARCH) {
            final SnapshotRecordResult snapshotRecordResult =
                    wireMockExtension.snapshotRecord(RecordSpec.forBaseUrl(ELASTICSEARCH_PREFIX));
            assertThat(snapshotRecordResult.getStubMappings()).hasSize(1);
            snapshotRecordResult
                    .getStubMappings()
                    .forEach(
                            mapping -> {
                                try {
                                    FileUtils.write(
                                            new File("target/" + mapping.getId().toString() + ".json"),
                                            mapping.toString(),
                                            "UTF-8");
                                } catch (IOException e) {
                                    log.error("Failed to write recorded stub mapping", e);
                                }
                            });
        }

        wireMockExtension.verify(
                WireMock.postRequestedFor(WireMock.urlMatching(ELASTICSEARCH_PREFIX + "/_bulk.*")));
    }

    private static String getElasticsearchUrl() {
        final String wiremockBaseUrl =
                wireMockExtension.getRuntimeInfo().getHttpBaseUrl() + ELASTICSEARCH_PREFIX;
        if (PROXY_ELASTICSEARCH) {
            final String elasticsearchContainerUrl =
                    "http://"
                            + elasticsearchContainer.getHost()
                            + ":"
                            + elasticsearchContainer.getMappedPort(9200);
            wireMockExtension.stubFor(
                    WireMock.post(WireMock.urlMatching(ELASTICSEARCH_PREFIX + "/_bulk.*"))
                            .willReturn(
                                    WireMock.aResponse()
                                            .proxiedFrom(elasticsearchContainerUrl)
                                            .withProxyUrlPrefixToRemove(ELASTICSEARCH_PREFIX))
                            .persistent());
        }
        return wiremockBaseUrl;
    }

    private List<Instant> produceUpdateRecords(
            Function<JsonNode, JsonNode> lineMapper, String topic, Instant endWM)
            throws IOException, ExecutionException, InterruptedException {

        List<Future<RecordMetadata>> eventSends;
        final Properties producerProperties = new Properties();
        // See, https://developer.ibm.com/articles/benefits-compression-kafka-messaging/
        producerProperties.put("compression.type", "snappy");
        producerProperties.put("linger.ms", 100);
        try (Producer<Long, String> producer =
                SHARED_KAFKA_TEST_RESOURCE
                        .getKafkaTestUtils()
                        .getKafkaProducer(LongSerializer.class, StringSerializer.class, producerProperties)) {

            final Spliterator<JsonNode> eventNodeSpliterator =
                    Spliterators.spliteratorUnknownSize(
                            EventDataStreamUtilities.parseJson("/update.events.json").iterator(),
                            Spliterator.ORDERED);

            final Stream<JsonNode> eventNodes = StreamSupport.stream(eventNodeSpliterator, false);

            eventSends =
                    eventNodes
                            .map(lineMapper)
                            .map(line -> toProducerRecord(topic, line))
                            .map(producer::send)
                            .collect(Collectors.toList());
            // Send a fake event at time endWM (this is necessary to position the end offsets when using
            // bounded kafka streams)
            producer.send(new ProducerRecord<>(topic, 0, endWM.toEpochMilli(), null, ""));
        }
        List<Instant> timestamps = new ArrayList<>();
        for (Future<RecordMetadata> f : eventSends) {
            timestamps.add(Instant.ofEpochMilli(f.get().timestamp()));
        }
        timestamps.add(endWM);
        return timestamps;
    }

    @NotNull
    private static ProducerRecord<Long, String> toProducerRecord(String topic, JsonNode line) {
        return new ProducerRecord<>(
                topic,
                0,
                Instant.parse(line.get(REV_TIMESTAMP_FIELD).textValue()).toEpochMilli(),
                null,
                line.toString());
    }

    private static UnaryOperator<JsonNode> allEventsBefore(Instant now, Duration offset) {
        return (JsonNode event) -> {
            ((ObjectNode) event).put(REV_TIMESTAMP_FIELD, now.plus(offset).toString());
            return event;
        };
    }
}
