# Cirrus Streaming Updater

This project defines a combination of two flink applications that process wiki-page-related update events
and map them to elastic search updates.

## Build and Deploy

### CI/CD

Both applications are shipped as docker containers. The container images are produced as follows:

1. **\[maven]** Run a regular maven build `./mvnw clean verify`
2. **\[blubber]** Copy the resulting JARs in an image based on [`docker-registry.wikimedia.org/flink`](https://gerrit.wikimedia.org/r/plugins/gitiles/operations/docker-images/production-images/+/refs/heads/master/images/flink/flink)

For deployment, helm is used :

1. **\[helm]** [chart](https://gerrit.wikimedia.org/r/plugins/gitiles/operations/deployment-charts/+/refs/heads/master/charts/flink-app/), 
   see [example](https://gerrit.wikimedia.org/r/plugins/gitiles/operations/deployment-charts/+/refs/heads/master/charts/flink-app/.fixtures/jvm_app.yaml)

