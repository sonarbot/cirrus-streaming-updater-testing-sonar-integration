package org.wikimedia.discovery.cirrus.updater.producer.graph;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.time.Instant;
import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

import org.apache.flink.streaming.runtime.streamrecord.StreamRecord;
import org.apache.flink.streaming.util.OneInputStreamOperatorTestHarness;
import org.apache.flink.streaming.util.ProcessFunctionTestHarnesses;
import org.apache.flink.types.Row;
import org.apache.flink.util.OutputTag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.wikimedia.discovery.cirrus.updater.common.serde.EventDataStreamUtilities;
import org.wikimedia.discovery.cirrus.updater.producer.model.InputEvent;

class RouteFetchFailuresTest {
    private final OutputTag<Row> tag =
            new OutputTag<>(
                    "errors",
                    EventDataStreamUtilities.buildTypeInfo(
                            ProducerGraphFactory.FETCH_FAILURE_STREAM,
                            ProducerGraphFactory.FETCH_FAILURE_SCHEMA_VERSION));
    private final RouteFetchFailures failureRouter = new RouteFetchFailures(tag, "my_pipeline");

    public static Stream<Arguments> provide_exceptions_for_test_errors() {
        return Stream.of(
                Arguments.arguments(new InvalidMWApiResponseException("bad response"), "MW_ERROR"),
                Arguments.arguments(new RevisionNotFoundException("not found"), "NOT_FOUND"),
                Arguments.arguments(new IOException("IO problem"), "NETWORK_ERROR"),
                Arguments.arguments(
                        new UncheckedIOException(new IOException("IO problem")), "NETWORK_ERROR"));
    }

    @ParameterizedTest
    @MethodSource("provide_exceptions_for_test_errors")
    void test_errors(Exception e, String expectedErrorType) throws Exception {
        try (OneInputStreamOperatorTestHarness<RetryContext<InputEvent, Row>, Row> testHarness =
                ProcessFunctionTestHarnesses.forProcessFunction(failureRouter)) {
            InputEvent event = new InputEvent();
            event.setRequestId(UUID.randomUUID().toString());
            event.setEventTime(Instant.now());
            event.setIngestionTime(Instant.now());
            event.setDomain("my_domain");
            event.setWikiId("my_db");
            event.setPageId(123L);
            event.setPageNamespace(2L);
            event.setRevId(234L);
            event.setPageTitle("MyTitle");
            testHarness.processElement(new RetryContext<>(0, event, null, e), 1L);
            StreamRecord<Row> error = testHarness.getSideOutput(tag).poll();
            assertThat(error).isNotNull();
            assertThat(error.getTimestamp()).isEqualTo(1L);
            Row r = error.getValue();
            assertThat(r.<Row>getFieldAs("meta").getField("domain")).isEqualTo(event.getDomain());
            assertThat(r.<Row>getFieldAs("meta").getField("request_id")).isEqualTo(event.getRequestId());
            assertThat(r.getField("page_id")).isEqualTo(event.getPageId());
            assertThat(r.getField("rev_id")).isEqualTo(event.getRevId());
            assertThat(r.getField("page_namespace")).isEqualTo(event.getPageNamespace());
            assertThat(r.getField("page_title")).isEqualTo(event.getPageTitle());
            assertThat(r.getField("database")).isEqualTo(event.getWikiId());
            assertThat(r.getField("original_ingestion_time")).isEqualTo(event.getIngestionTime());
            assertThat(r.getField("original_event_time")).isEqualTo(event.getEventTime());
            assertThat(r.getField("error_type")).isEqualTo(expectedErrorType);
            assertThat(r.getField("error_message")).isEqualTo(e.getMessage());
        }
    }

    @Test
    void test_events_are_propagated() throws Exception {
        try (OneInputStreamOperatorTestHarness<RetryContext<InputEvent, Row>, Row> testHarness =
                ProcessFunctionTestHarnesses.forProcessFunction(failureRouter)) {
            InputEvent event = new InputEvent();
            event.setRequestId(UUID.randomUUID().toString());
            Row row = Row.of();
            testHarness.processElement(new RetryContext<>(1, event, row, null), 1L);
            List<StreamRecord<? extends Row>> streamRecords = testHarness.extractOutputStreamRecords();
            assertThat(streamRecords).isNotEmpty();
            StreamRecord<? extends Row> streamRecord = streamRecords.get(0);
            assertThat(streamRecord.getTimestamp()).isEqualTo(1);
            assertThat(streamRecord.getValue()).isEqualTo(row);
        }
    }

    @Test
    void test_unexpected_error_are_not_propagated() throws Exception {
        try (OneInputStreamOperatorTestHarness<RetryContext<InputEvent, Row>, Row> testHarness =
                ProcessFunctionTestHarnesses.forProcessFunction(failureRouter)) {
            InputEvent event = new InputEvent();
            event.setRequestId(UUID.randomUUID().toString());
            RetryContext<InputEvent, Row> tuple3 =
                    new RetryContext<>(0, event, null, new IllegalStateException("unexpected"));
            assertThatThrownBy(() -> testHarness.processElement(tuple3, 1L))
                    .isInstanceOf(IllegalArgumentException.class);
        }
    }
}
