package org.wikimedia.discovery.cirrus.updater.producer.sink;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.io.IOException;
import java.time.Instant;
import java.util.stream.Stream;

import javax.annotation.Nonnull;

import org.apache.flink.api.common.typeutils.TypeSerializer;
import org.apache.flink.api.java.typeutils.runtime.TestDataOutputSerializer;
import org.apache.flink.types.Row;
import org.assertj.core.api.Condition;
import org.junit.jupiter.api.Named;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateFields;
import org.wikimedia.discovery.cirrus.updater.common.serde.EventDataStreamUtilities;
import org.wikimedia.discovery.cirrus.updater.producer.model.ChangeType;
import org.wikimedia.discovery.cirrus.updater.producer.model.InputEvent;
import org.wikimedia.eventutilities.flink.EventRowTypeInfo;

import com.fasterxml.jackson.databind.ObjectMapper;

class UpdateRowEncoderTest {

    private final ObjectMapper objectMapper = new ObjectMapper();

    private final EventRowTypeInfo outputTypeInfo = EventDataStreamUtilities.buildUpdateTypeInfo();
    private final UpdateRowEncoder encoder = new UpdateRowEncoder(outputTypeInfo, objectMapper);

    @Test
    void encodeAndSerialize() throws IOException {
        final Instant now = Instant.now();
        final EventRowTypeInfo outputTypeInfo = EventDataStreamUtilities.buildUpdateTypeInfo();
        final UpdateRowEncoder encoder = new UpdateRowEncoder(outputTypeInfo, objectMapper);

        final InputEvent inputEvent = createInputEvent(now);

        final Row row =
                encoder.encodeCirrusBuildDoc(
                        EventDataStreamUtilities.parseJson("/wiremock/__files/revision.551141.json")
                                .at("/query/pages/0"),
                        inputEvent);

        assertThat(row.getField("dt")).isEqualTo(now);
        assertThat(row.getField("page_id")).isEqualTo(147498L);
        assertThat(row.getField("rev_id")).isEqualTo(551141L);
        assertThat(row.getField("domain")).isEqualTo("my.domain.local");
        assertThat(row.getField("wiki_id")).isEqualTo("mywiki_id");
        assertThat(row.getField("index_name")).isEqualTo("testwiki_general");
        assertThat(row.getField("cluster_group")).isEqualTo("omega");

        final TypeSerializer<Row> rowSerializer = outputTypeInfo.createSerializer(null);
        final TestDataOutputSerializer target = new TestDataOutputSerializer(4096);
        rowSerializer.serialize(row, target);

        assertThat(target.length()).isPositive();
    }

    @Test
    void encodeFailsIfInputIsIncomplete() {
        assertThatThrownBy(
                        () -> {
                            encoder.encodeCirrusBuildDoc(
                                    EventDataStreamUtilities.parseJson(
                                            "/wiremock/__files/revision.551141.no-metadata.json"),
                                    createInputEvent(Instant.now()));
                        })
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void encodeFailsIfPageIdDoesNotMatch() {
        final InputEvent inputEvent = createInputEvent(Instant.now());
        inputEvent.setPageId(-1L);

        assertThatThrownBy(
                        () -> {
                            encoder.encodeCirrusBuildDoc(
                                    EventDataStreamUtilities.parseJson("/wiremock/__files/revision.551141.json")
                                            .at("/query/pages/0"),
                                    inputEvent);
                        })
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void encodeFailsIfRevIdDoesNotMatch() {
        final InputEvent inputEvent = createInputEvent(Instant.now());
        inputEvent.setRevId(-1L);

        assertThatThrownBy(
                        () -> {
                            encoder.encodeCirrusBuildDoc(
                                    EventDataStreamUtilities.parseJson("/wiremock/__files/revision.551141.json")
                                            .at("/query/pages/0"),
                                    inputEvent);
                        })
                .isInstanceOf(IllegalArgumentException.class);
    }

    @ParameterizedTest
    @MethodSource("encodeInputEventParameters")
    void encodeInputEvent(InputEvent inputEvent, Condition<Row> condition) {
        assertThat(encoder.encodeInputEvent(inputEvent)).has(condition);
    }

    static Stream<Arguments> encodeInputEventParameters() {
        final Instant now = Instant.now();
        final InputEvent delete = createInputEvent(now);
        delete.setChangeType(ChangeType.PAGE_DELETE);
        final InputEvent revBasedUpdate = createInputEvent(now);
        revBasedUpdate.setChangeType(ChangeType.REV_BASED_UPDATE);
        final InputEvent unknown = createInputEvent(now);
        unknown.setChangeType(null);
        return Stream.of(
                Arguments.of(
                        Named.of(UpdateFields.OPERATION_UPDATE_REVISION, revBasedUpdate),
                        new Condition<Row>(
                                row ->
                                        UpdateFields.getOperation(row).equals(UpdateFields.OPERATION_UPDATE_REVISION),
                                "Unexpected operation")),
                Arguments.of(
                        Named.of(UpdateFields.OPERATION_DELETE, delete),
                        new Condition<Row>(
                                row -> UpdateFields.getOperation(row).equals(UpdateFields.OPERATION_DELETE),
                                "Unexpected operation")));
    }

    @Nonnull
    private static InputEvent createInputEvent(Instant now) {
        final InputEvent inputEvent = new InputEvent();
        inputEvent.setEventTime(now);
        inputEvent.setPageId(147498L);
        inputEvent.setRevId(551141L);
        inputEvent.setDomain("my.domain.local");
        inputEvent.setWikiId("mywiki_id");
        inputEvent.setChangeType(ChangeType.REV_BASED_UPDATE);
        return inputEvent;
    }
}
