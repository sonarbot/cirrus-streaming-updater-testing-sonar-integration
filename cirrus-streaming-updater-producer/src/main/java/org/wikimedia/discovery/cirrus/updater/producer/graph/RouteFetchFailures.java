package org.wikimedia.discovery.cirrus.updater.producer.graph;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.collect.ImmutableList.copyOf;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.Arrays;
import java.util.List;

import org.apache.flink.streaming.api.functions.ProcessFunction;
import org.apache.flink.types.Row;
import org.apache.flink.util.Collector;
import org.apache.flink.util.OutputTag;
import org.wikimedia.discovery.cirrus.updater.producer.model.InputEvent;
import org.wikimedia.eventutilities.flink.EventRowTypeInfo;

public class RouteFetchFailures extends ProcessFunction<RetryContext<InputEvent, Row>, Row> {
    private final OutputTag<Row> errorOutputTag;
    private final String pipeline;
    private final EventRowTypeInfo rowTypeInfo;

    public RouteFetchFailures(OutputTag<Row> errorOutputTag, String pipeline) {
        this.errorOutputTag = errorOutputTag;
        checkArgument(
                errorOutputTag.getTypeInfo() instanceof EventRowTypeInfo,
                "errorOutputTag must have a TypeInfo of type EventRowTypeInfo");
        rowTypeInfo = (EventRowTypeInfo) errorOutputTag.getTypeInfo();
        this.pipeline = pipeline;
    }

    @Override
    public void processElement(
            RetryContext<InputEvent, Row> o, Context context, Collector<Row> collector) {
        // We expect an Either monad here between output and failure.
        checkArgument(
                o.getOutput().isPresent() != o.getFailure().isPresent(),
                "RetryContext should either have an output or a failure.");
        o.getOutput().ifPresent(collector::collect);
        o.getFailure()
                .ifPresent(
                        ex -> {
                            InputEvent event = o.getInput();
                            Row failure = rowTypeInfo.createEmptyRow();
                            failure.setField("pipeline", pipeline);
                            failure.setField("database", event.getWikiId());
                            failure.setField("page_id", event.getPageId());
                            failure.setField("page_title", event.getPageTitle());
                            failure.setField("rev_id", event.getRevId());
                            failure.setField("page_namespace", event.getPageNamespace());
                            failure.setField("original_event_stream", event.getEventStream());
                            failure.setField("original_event_time", event.getEventTime());
                            failure.setField("original_ingestion_time", event.getIngestionTime());
                            Row meta = rowTypeInfo.createEmptySubRow("meta");
                            meta.setField("domain", event.getDomain());
                            meta.setField("request_id", event.getRequestId());
                            failure.setField("meta", meta);

                            ErrorType errorType =
                                    Arrays.stream(ErrorType.values())
                                            .filter(er -> er.matches(ex))
                                            .findFirst()
                                            .orElseThrow(
                                                    () ->
                                                            new IllegalArgumentException(
                                                                    "Unsupported exception type: " + ex.getClass(), ex));

                            failure.setField("error_type", errorType.name());
                            failure.setField("error_message", ex.getMessage());
                            context.output(errorOutputTag, failure);
                        });
    }

    enum ErrorType {
        NOT_FOUND(RevisionNotFoundException.class),
        NETWORK_ERROR(IOException.class, UncheckedIOException.class),
        MW_ERROR(InvalidMWApiResponseException.class);

        private final List<Class<? extends Throwable>> exceptionTypes;

        @SafeVarargs
        ErrorType(Class<? extends Throwable>... exceptionType) {
            this.exceptionTypes = copyOf(exceptionType);
        }

        public boolean matches(Throwable exception) {
            return exceptionTypes.stream().anyMatch(e -> e.isAssignableFrom(exception.getClass()));
        }
    }
}
